# CANAL DBA

[Pagina actual](http://www.canaldba.com/)

## [Gitlab page](https://dmob-ar.gitlab.io/customers/canaldba/)


### Getting started

```
mkdir canaldba
cd canaldba
git clone git@gitlab.com:dmob-ar/customers/canaldba.git
```

### Built-In Server

> Prerequisitos: hugo

```
cd site
hugo server
```

También hay un `Makefile` para buildear el sitio en local.

```
make dev
# o
make dev-remote
```

Hay un docker-compose, para buildear con docker.

```
docker-compose up
```


## About && License

CanalDBA 2018-2022.

Desarrollado con la colaboración de [DMOB Dev](https://dmob.dev/).





