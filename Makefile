SHELL=/bin/bash

IP=$$(hostname -I | cut -d' ' -f1)

dev:
	cd site/ && \
	hugo serve --config config.toml --baseURL="http://localhost"

# This method is for exposing the site to be accessed from a remote machine
dev-remote:
	cd site/ && \
		hugo serve --bind=0.0.0.0 --baseURL="http://$(IP)"  --port=1313 --config config-local.toml
