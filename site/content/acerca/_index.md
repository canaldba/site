+++
title = "Acerca de Nosotros"
type = "about"
+++

¡Hey! Si llegaste aca es porque querés saber de nosotros, el **CanalDBA**, una comunidad de Data Nerds del Río de la Plata (en su mayoría). Nuestro propósito es interconectarnos fuera del ámbito de las compañías donde trabajamos, acerca de temas variados relacionados a los motores de base de datos bajo licencia de Software Libre. 


Entre nuestras filas, tenemos gente que trabaja en compañías como Percona, Pythian, Oracle, OnGres, Teradata, ViaDB, instituciones gubernamentales, etc (iremos agregando a medida que se vayan sumando).

## SQL o NoSQL, esa es la cuestión

Acá no discriminamos, tenemos gente que trabaja con variadas tecnologías y hasta incluso algún que otro infiltrado. A diferencia de otras comunidades, no nos enfocamos en una tecnología, pero si tratamos de que el cluster de gente sea local (más que nada para organizar las meetups).

## Localización

Nuestra area geográfica se emplaza principalmente en Buenos Aires y Montevideo (y todo lo que hay entre medio). Hasta incluso se propuso la idea de hacer un Buquebus Day.

El COVID-19 cambió un poco los planes, pero aún se hacen encuentros via stream.

## GIT

Tenemos una organización en:

- [Github](https://github.com/canalDBA)
- [Gitlab](https://gitlab.com/canaldba)


Si lo que te interesa es trabajar en conjunto con otras personas de la comunidad, ¡sumate y buscá un proyecto en común!

