+++
author = "Comunidad"
categories = ["Webinar"]
tags = [""]
date = "2020-04-09"
description = ""
img = "/img/webinar-min.jpg"
linktitle = ""
title = "Webinar Kickoff"
type = "post"

+++
## Descripción

Arrancamos un ciclo de charlas online. El formato de este primer encuentro será de 2 bloques. Enterate tanto en nuestro grupo de Meetup o en Slack del link de zoom a dónde vamos a estar conectados el día del evento. Si no querés entrar zoom, podés seguir la charla en nuestro canal de youtube.

Fecha: Martes 7 de abril a las 17:30 GMT-3

[Link de la charla](https://www.youtube.com/watch?v=Cw1Nngpnlyo)

## Charlas

    - Lab Testing Odyssey on stressed envorinment.
    - Lab Primero pasos con dbdeployer

Más info en:

    - [Meetup](https://www.meetup.com/es/CanalDBA-MVD-Meetup-Group/)
    - [Slack](https://canaldba.slack.com/join/shared_invite/zt-4hzvukwn-IUfhS06dLXFPl3ohcCfKeQ#/shared-invite/email)
    - [Youtube](https://www.youtube.com/channel/UC0S-8mapRuelUNQxDPi7vbw)


