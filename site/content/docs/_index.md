---
title : "Docs"
type : "docs"
docs :
    - name : "Clickhouse ()"
      link : "https://clickhouse.com/docs/en/"
      img  : "img/logos/docs/clickhouse.png"

    - name : "MongoDB"
      link : "https://docs.mongodb.com/"
      img  : "img/logos/docs/mongodb.png"

    - name : "Galera / XtraDB"
      link : ""

    - name : "MySQL"
      link : "https://dev.mysql.com/doc/"
      img  : "img/logos/docs/mysql_logo2.png"

    - name : "PostgreSQL (Relacional)"
      link : "https://www.postgresql.org/docs/"
      img  : "img/logos/docs/postgresql.png"

    - name : "PostgreSQL-XL"
      link : "https://www.postgres-xl.org/documentation/"

    - name : "CockRoachDB"
      link : "https://www.cockroachlabs.com/docs/"
      img  : "img/logos/docs/cockroachdb.png"
---


Aqui encontrarás los recursos necesarios para adentrarte en las tecnologías que esta comunidad maneja (WIP).