+++
type = "eventos"
tipo = "evento"
author = ""
date = "2017-11-01"
title = "Evento Percona/Pythian"
description = "Evento Percona/Pythian"
featured = ""
featuredpath = ""
featuredalt = ""
categories = [""]
linktitle = ""
format = ""
link = "#"
+++

## Descripción

Noviembre 16, 2017. Montevideo.

Más info en: [Meetup link](https://www.meetup.com/The-Montevideo-MySQL-Meetup-Group/events/244513631/)

## Call for papers

[Formulario](https://docs.google.com/forms/d/e/1FAIpQLScRDlL4EUZnmw7rIxN82ZoZPrIatMT4-PYtJ7Msm89yK9NStg/viewform)

