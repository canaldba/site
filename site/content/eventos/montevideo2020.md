+++
type = "eventos"
tipo = "evento"
author = ""
date = "2020-03-04"
title = "[POSTERGADO] Nos juntamos en marzo del 2020 en Montevideo!"
description = "[POSTERGADO] Nos juntamos en marzo del 2020 en Montevideo!"
featured = ""
featuredpath = ""
featuredalt = ""
categories = [""]
linktitle = ""
format = ""
link = "#"
+++

Con motivo de prevención del coronavirus, debemos postergar la realización del evento.

Pero que no decaiga, lo más lindo de la comunidad de CanalDBa pasa en nuestro canal de Slack

## Unite usando este [link](https://canaldba.slack.com/join/shared_invite/zt-4hzvukwn-IUfhS06dLXFPl3ohcCfKeQ#/shared-invite/email) o bien mandame un mail a mateo@canaldba.com

# Descripción

Edificio Victoria Plaza, Montevideo, Uruguay #PedidosYa

No te olvides inscribirte (Por la comida y bebida) en Meetup, o mandarnos un mail (datos en el link).

Charlas

    - Infraestructura de PedidosYa (Equipo de Data de PedidosYa)
    - MVCC+VACUUM en Postgres (Martin Marques, 2ndQuadrant)
    - Memory Performance (Marcos Albe, Percona)

Luego habrá espacio para lighting talks.

Tiempo para el morfi y bebidas sponsoreadas por PedidosYa

Más info en: [CanalDBA Meetup Group](https://www.meetup.com/es/CanalDBA-MVD-Meetup-Group/)
