+++
type = "eventos"
tipo = "evento"
author = ""
date = "2019-10-10"
title = "Nos vemos en Nerdear.la 2019!"
description = "Nos vemos en Nerdear.la 2019!"
featured = ""
featuredpath = ""
featuredalt = ""
categories = [""]
linktitle = ""
format = ""
link = "#"
+++

## Descripción

Ciudad Cultural Konex, CABA, Argentina #Sysarmy

Más info en: [Nerdear.la](https://nerdear.la/agenda/)
