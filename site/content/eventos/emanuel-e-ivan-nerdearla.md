+++
type = "eventos"
tipo = "evento"
author = ""
date = "2018-08-30"
title = "Emanuel Calvo e Iván Groenewold en Nerdear.la!"
description = "Emanuel Calvo e Iván Groenewold en Nerdear.la!"
featured = ""
featuredpath = ""
featuredalt = ""
categories = [""]
linktitle = ""
format = ""
link = "#"
+++

## Descripción

Centro Cultural San Martín, #Syarmy.

Más info en: [Nerdear.la](https://nerdear.la/#speakers)


